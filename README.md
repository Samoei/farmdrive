# FarmDrive Software Engineering Test #

This Engineering test has been solved by using Python 3.4.3

All the questions are self executable meaning they have a main method with some sample data that be run as a module in python

The Code is heavily commented to so as to provide better understanding when someone else is going through it.



### Step to run solutions ? ###

* Clone this repo `git clone https://Samoei@bitbucket.org/Samoei/farmdrive.git`
* Change directory to farmdrive by `cd farmdrive`
* Run `python Question_1.py` to view question 1 solution
* Run `python Question_2.py` to view question 2 solution
* Run `python Question_3.py` to view question 3 solution