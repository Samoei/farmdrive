N=8
def parking_spaces(start, n, increment=1):
    "Return a tuple of n locations, starting at start and incrementing by increment."
    space = [0]*n
    i = 0
    while i < n:
        space[i]= start + increment*i
        i = i + 1
    return tuple(space)

# print(parking_spaces(26,2))
# print(parking_spaces(41,2,8))
# print(parking_spaces(0,8)+ parking_spaces(8,2,7)+parking_spaces(16,2,7)+parking_spaces(24,2,7)+parking_spaces(32,2,7)+parking_spaces(40,2,7)+parking_spaces(48,2,7)+parking_spaces(56,8))

def parking_lot(cars, N=N):
    goal = ('@', (31,)) # fixed value aka exit space
    walls_loci = parking_spaces(0,8)+ parking_spaces(8,2,7)+parking_spaces(16,2,7)+parking_spaces(24,1,7)+parking_spaces(32,2,7)+parking_spaces(40,2,7)+parking_spaces(48,2,7)+parking_spaces(56,8) #wall has less value
    walls = ('|',walls_loci) #fixed value
    return (goal,)+cars+(walls,) # use (goal,) to add tuple, not goal +

def solve_parking_puzzle(start, N=N):
    return get_path(start, movement, is_goal)


# puzzle1 = (
#  ('@', (31,)),
#  ('*', (26, 27)),
#  ('G', (9, 10)),
#  ('Y', (14, 22, 30)),
#  ('P', (17, 25, 33)),
#  ('O', (41, 49)),
#  ('B', (20, 28, 36)),
#  ('A', (45, 46)),
#  ('|', (0, 1, 2, 3, 4, 5, 6, 7, 8, 15, 16, 23, 24, 32, 39,
#         40, 47, 48, 55, 56, 57, 58, 59, 60, 61, 62, 63)))
'''

| | | | | | | |
| G G . . . Y |
| P . . B . Y |
| P * * B . Y @
| P . . B . . |
| O . . . A A |
| O . . . . . |
| | | | | | | |
| | | | | | | |
| G G . . . . |
| P . . . . . |
| P * * . . . @
| P . . B . Y |
| O A A b . Y |
| O . . b . Y |
| | | | | | | |'''


def movement(state):
    results = {} # (state, action)
    for c in state:
        if c[0]!='|' and c[0]!='@':
            loc = c[1]
            occupied = occupied_loci(state)

            if is_hori(loc): # could move right and left
                left,right,length = boundry(loc)
                l = 1
                while (left-l) not in occupied:
                    l = l + 1
                max_dec_step = l-1

                if max_dec_step != 0:
                    for i in range(1,max_dec_step+1):
                        newloc = parking_spaces(left-i,length) #update c[0] location, update in a tuple
                        action = (c[0],-i)
                        newc = (c[0],newloc)
                        new_state = newstate(state,c,newc)
                        results[new_state] = action


                r = 1
                while (right+r) not in occupied:
                    r = r + 1
                max_inc_step = r-1

                if max_inc_step != 0:
                    for i in range(1,max_inc_step+1):
                        newloc = parking_spaces(left+i,length) #update state # still start with left
                        action = (c[0],i)
                        newc = (c[0],newloc)
                        new_state = newstate(state,c,newc)
                        results[new_state] = action

            else:
                up, down,length = boundry(loc)

                l = 1
                while (up - N*l) not in occupied:
                    l = l + 1

                max_dec_step = l-1

                if max_dec_step != 0:
                    for i in range(1,max_dec_step+1):
                        newloc = parking_spaces(up-N*i,length,N) #update c[0] location, update in a tuple, both start with up
                        action = (c[0],-N*i)
                        newc = (c[0],newloc)
                        new_state = newstate(state,c,newc)
                        results[new_state] = action

                r = 1
                while (down + N*r) not in occupied:
                    r = r + 1
                max_inc_step = r-1

                if max_inc_step != 0:
                    for i in range(1,max_inc_step+1):
                        newloc = parking_spaces(up+N*i,length,N) #update state, start with up
                        action = (c[0],N*i) # (B,8) --> NEW LOC = parking_spaces(DOWN+8, 3, N)
                        newc = (c[0],newloc)
                        new_state = newstate(state,c,newc)
                        results[new_state] = action
    return results


def newstate(state,c,newc):
    results = []
    for s in state:
        if s != c:
            results.append(s)
        else:
            results.append(newc)
    return tuple(results)

def occupied_loci(state): # @ is not occupied, exit
    results = []
    for c in state:
        if c[0] != '@':
            for l in c[1]:
                results.append(l)
    return sorted(results)

def boundry(loc_tuple):
    return min(loc_tuple), max(loc_tuple),len(loc_tuple)

def is_hori(loc_tuple): #c[1] = loc_tuple
    return (loc_tuple[1]-loc_tuple[0]) == 1


def get_path(start, successors, is_goal):
    if is_goal(start):
        return []
    explored = set()
    frontier = [ [start] ]
    while frontier:
        path = frontier.pop(0)
        s = path[-1]
        for (state,action) in successors(s).items():
            if state not in explored:
                explored.add(state)
                path2 = path + [action, state]
                actions = path_actions(path2)
                if is_goal(state):
                    return actions
                else:
                    frontier.append(path2)
    return []

def is_goal(state):
     for c in state:
         if c[0] == '*':
             return 31 in c[1]

# print is_goal(puzzle1)

def path_actions(path):
    "Return a list of actions in this path."
    return path[1::2]


if __name__ == '__main__':
    trm_parking_lot = parking_lot((
    ('*', parking_spaces(26, 2)),
    ('G', parking_spaces(9, 2)),
    ('Y', parking_spaces(14, 3, N)),
    ('P', parking_spaces(17, 3, N)),
    ('O', parking_spaces(41, 2, N)),
    ('B', parking_spaces(20, 3, N)),
    ('A', parking_spaces(45, 2))))

    junction_parking_lot = parking_lot((
    ('*', parking_spaces(26, 2)),
    ('B', parking_spaces(20, 3, N)),
    ('P', parking_spaces(33, 3)),
    ('O', parking_spaces(41, 2, N)),
    ('Y', parking_spaces(51, 3))))

    prestige_parking_lot = parking_lot((
    ('*', parking_spaces(25, 2)),
    ('B', parking_spaces(19, 3, N)),
    ('P', parking_spaces(36, 3)),
    ('O', parking_spaces(45, 2, N)),
    ('Y', parking_spaces(49, 3))))
    print(solve_parking_puzzle(prestige_parking_lot,N=N))