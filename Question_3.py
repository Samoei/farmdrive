sample_graph = {
         'a': {
               'b': 1.1,
               'c': 1.01,
               'd': 5.23
         },
         'b': {
               'a': 4.8,
               'c': 3.8,
               'd': 8.0
         },
         'c':{
               'a': 0.01,
               'b': 4.11,
               'd': 7.74
         },
         'd': {
               'a': 8.45,
               'b': 1.22,
               'c': 2.10
         }
      }

def update_accessible_path(graph, i):
    global path, marked
    for node in graph[i]:
        if (node not in marked):
            marked[node] = True
            path.append(node)
            update_accessible_path(graph, node)
            path.append(i)

def love_strength(graph, i, j):

    global path, marked
    path = [i]
    marked = {}
    update_accessible_path(graph, i)
    if (j in path):
        # path ends in i, we needs to add nodes to end in j
        posJ = 0
        while(path[posJ] != j):
            posJ += 1
        return path + path[1:posJ+1]
    else:
        return None

def strength_of_path(graph, path):
    max_love = -float('inf')
    for n1, n2 in zip(path[:-1], path[1:]):
        love = graph[n1][n2]
        if love > max_love:
            max_love = love
    return max_love

def graderTest():
    other = love_strength(sample_graph, 'a', 'b')
    print("path", other)
    print("score", strength_of_path(sample_graph, other))

if __name__ == '__main__':
   best_path = love_strength(sample_graph, 'a', 'b')
   print("Best Path", best_path)
   print("Best Path Score", strength_of_path(sample_graph, best_path))
