from itertools import permutations


def solve_puzzle():
	days = {'Monday': 1, 'Tuesday': 2, 'Wednesday': 3, 'Thursday': 4, 'Friday': 5}
	names_in_order = []

	perms = list(permutations(days.values()))

	names_in_order = next(
		{'Hamming': hamming, 'Knuth': knuth, 'Minsky': minksy, 'Simon': simon, 'Wilkes': wilkes}

		# USE PEOPLE TO DETERMINE THE TRUTH
		for hamming, knuth, minksy, simon, wilkes in perms
		# FACT 6. Knuth arrived the day after Simon.
		if simon + 1 == knuth

		# USE ITEMS BOUGHT TO DETERMINE THE TRUTH
		for laptop, droid, tablet, iphone, other in perms
		# FACT 1. The person who arrived on Wednesday bought the laptop.
		if days['Wednesday'] == laptop and
		# FACT 8. The person who arrived on Friday didn't buy the tablet.
		days['Friday'] != tablet and
		# FACT 12. Either the person who bought the iphone or the person who bought the tablet arrived on Tuesday.
		(iphone == days['Tuesday'] or tablet == days['Tuesday'])

		# USE  OCCUPATIONS TO DETERMINE THE TRUTH
		for programmer, writer, manager, designer, unknown in perms
		# FACT 2. The programmer is not Wilkes.
		if programmer != wilkes and
		# FACT 4. The writer is not Minsky.
		writer != minksy and
		# FACT 3. Of the programmer and the person who bought the droid, one is Wilkes and the other is Hamming.
		((droid == wilkes and programmer == hamming) or (droid == hamming and programmer == wilkes)) and
		# FACT 5. Neither Knuth nor the person who bought the tablet is the manager.
		manager != knuth and manager != tablet and
		# FACT 7. The person who arrived on Thursday is not the designer.
		days['Thursday'] != designer and
		# FACT 9. The designer didn't buy the droid.
		designer != droid and
		# FACT 11. Of the person who bought the laptop and Wilkes, one arrived on Monday and the other is the writer.
		((laptop == days['Monday'] and wilkes == writer) or (laptop == writer and days['Monday'] == wilkes)) and
		# FACT 10. Knuth arrived the day after the manager.
		knuth == manager + 1)
	return names_in_order


def logic_puzzle():
	arrivals = solve_puzzle()
	return sorted(arrivals.keys(), key=lambda x: arrivals[x])


if __name__ == '__main__':
	print(logic_puzzle())
